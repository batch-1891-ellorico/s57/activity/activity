let collection = [];

// Write the queue functions below.


// 1. Output all the elements of the queue
function print () {
    return collection
}



// 2. Adds element to the rear of the queue
function enqueue (element) {
    collection[collection.length] = element;
    return collection
};


// 3. Removes element from the front of the queue
function dequeue () {
    collection.splice(0, 1)
    return collection
}


// 4. Show element at the front
function front () {
    return collection[0]
}


// 5. Show the total number of elements
function size () {
    let total = 0;
    for (let i of collection) {
        total++
    }
    return total
}


// 6. Outputs a Boolean value describing whether queue is empty or not
function isEmpty(){
    return collection.length == 0
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};